FROM docker:latest

LABEL MAINTAINER dev@lallemand.fr

RUN apk add --update py-pip && \
    pip install docker-compose
